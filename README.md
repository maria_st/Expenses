User name is an IP address.
This is just a sample app to play with Node.js and Express using MongoDB and EJS as view engine.
Written with Vanilla JS:) and jQuery on client side.
Charts generated with D3.js.
Responsive web design.
Optimistic UI.
Deployed on AWS.
Note: This is just a very simple example plenty of room for improvement.