'use strict';

function drawChart(type) {
	$(".total .add-receipt").hide();

	$(".main").html($("template[data-template='chart']").html());

	const main_height = $(".main").height() - $(".main").height()/4, header = 32, $svg = $(".main svg");
	$(".main .receipt[data-action='chart']").height(main_height+'px');
	$svg.height((main_height-header)+'px');

	const width = $svg.width(), height = $svg.height(), color = d3.scaleOrdinal().domain(chart_colors[0]).range(chart_colors[1]);
	// prepare data for pie chart
	const o_chart_0 = [];
	o_receipt.forEach(function(i){
		if (i.total != 0){
			o_chart_0.push({group:i.group.name,icon:i.group.icon,total:i.total,percentage:i.total/o_user.total*100});
		};
	});
	const o_chart = o_chart_0.groupBy('group',o_user.total);


	function drawPie(){
		const radius_reduce = 80, inner_radius = 60, radius = (width - radius_reduce)/2;

		var g = d3.select(".main svg").data([o_chart]).append('g').attr('transform', 'translate('+ (width/2) +','+ (height/2) +')');
		// generate arcs
		var arc = d3.arc().innerRadius(inner_radius).outerRadius(radius);
		var pie = d3.pie().sort(null).value(function(d) { return d.total; });
		// append each slice with text
		var slice = g.selectAll("g.slice").data(pie).enter().append("svg:g").attr("class", "slice");
		slice.append("svg:path").attr("fill", function(d, i) { return color(d.data.group); } ).attr("d", arc);
		slice.append("svg:text").attr("transform", function(d) { d.innerRadius = inner_radius; d.outerRadius = radius;
			return "translate(" + arc.centroid(d) + ")"}).attr("text-anchor", "middle").attr('class','svg-text')
			.text(function(d, i) { return roundNumber(d.data.percentage)+'%'; });
		// total in the middle
		g.append("text").attr("text-anchor", "middle").attr('class','svg-text').attr('font-size','16px').attr('y', -4).text('Total');
		g.append("text").attr("text-anchor", "middle").attr('class','svg-text').attr('font-size','16px').attr('y', 14).html(roundNumber(o_user.total) +'&nbsp;&euro;');
	};

	function drawBar() {
		var margin = {top: 20, right: 20, bottom: 30, left: 40}, l_width = width-margin.left-margin.right, l_height = height-margin.top-margin.bottom
			, g = d3.select(".main svg").append('g').attr('transform', 'translate('+ margin.left +','+ margin.top +')');
		// axis
		var x = d3.scaleBand().rangeRound([0, l_width]).padding(0.1)
			, y = d3.scaleLinear().rangeRound([l_height, 0]);
		x.domain(o_chart.sort(function(a,b){return b.total - a.total}).map(function(d) { return d.group; }));
		y.domain([0, d3.max(o_chart, function(d) { return d.total; })]);

		g.append("g")
		.attr("class", "axis-x")
		.attr("transform", "translate(0," + l_height + ")")
		.call(d3.axisBottom(x));

		g.append("g")
		.attr("class", "axis-y")
		.call(d3.axisLeft(y).ticks(10))
		.append("text")
		.attr("transform", "rotate(-90)")
		.attr("y", 6)
		.attr("dy", "0.71em")
		.attr("text-anchor", "end")
		.text("Total");

		g.selectAll(".bar")
		.data(o_chart)
		.enter().append("rect")
		.attr("class", "bar")
		.attr("x", function(d) { console.log(x(d.group)); return x(d.group); })
		.attr("y", function(d) { return y(d.total); })
		.attr("width", x.bandwidth())
		.attr("height", function(d) { return l_height - y(d.total) })
		.attr('fill', function(d) { return color(d.group)})

		g.selectAll(".text")  		
		.data(o_chart)
		.enter().append("text")
		.attr("class","label")
		.attr('class','svg-text')
		.attr("x", (function(d) { return x(d.group)+x.bandwidth()/3 }))
		.attr("y", function(d) { return y(d.total)+6 })
		.attr("dy", ".75em")
		.text(function(d) { return roundNumber(d.total) }); 

	};
	
	let l_return = drawPie;
	if (type == 'bar'){ l_return = drawBar; };
	return l_return;
	
};


