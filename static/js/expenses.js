'use strict';
var o_user = [], userId = '', userIp = '';
const o_receipt = [], group_list = [], chart_colors = [];


// ajax
function ajaxGlobal(type,data,console_text) {
	$.ajax({
		type: 'POST',
		url: '/expenses',
		contentType: "application/json; charset=utf-8",
		dataType: 'json',
		data: JSON.stringify(data),
		success: function(data) {			
			// for testing
			console.log(console_text);
			if (type == 'onload'){
				//console.log(data);
				onLoad(data);										
			};
		},
		error: function (xhr) {
			console.log(data.err) 
			console.log("err",xhr.status,xhr.statusText,xhr.responseText);
		}
	});	
};

// on load
$.getJSON('https://json.geoiplookup.io/', function(data) {
	let post = {type:'onload',data:data};
	ajaxGlobal('onload',post,'ajax onload');
});
function onLoad(data) {
	o_user = data.user[0], userId = o_user._id, userIp = o_user.ip;
	var icons = [], colors = [];
	data.group_list.forEach(function(i){
		icons.push(i.name);
		colors.push(i.color);
	});
	chart_colors.push(icons); chart_colors.push(colors);

	group_list.push(data.group_list);
	data.receipt.forEach(function(i){
		o_receipt.push(i);
	});
	$(".user-name").html(userIp);
	drawReceipt();

	o_user.update = function(field){
		this[field.field] = field.value;
	};

};

// add receipt
$(".add-receipt").click(function(){
	hideTooltip();	
	var receipt = {_id:getUID(),group:{name:'Groceries',icon:'shopping_cart'},expense:[{_id:getUID(),name:'',sum:0}],total:0,user_id:userId}
		, post = {type:'add-receipt',receipt:receipt,userIp:userIp};
	o_receipt.add(receipt);
	ajaxGlobal('add-receipt',post,'ajax add receipt');
});

// update receipt: group
$(".dropdown[data-action='group']").on('click', '.dropdown-li', function(){
	var $this = $(this), $dropdown = $this.closest(".dropdown"), $id = $dropdown.attr("data-id");
	closeDropdown();
	o_receipt.update($id,{field:'group',value:{name:$this.attr("data-name"),icon:$this.attr("data-icon")}},true);
	var post = {type:'update-receipt',_id:$id,update:{field:'group',value:{name:$this.attr("data-name"),icon:$this.attr("data-icon")}},userIp:userIp};
	ajaxGlobal('update-receipt',post,'ajax update receipt');
});

// add expense
$(".expenses-form").on('click', '.receipt .add-expense', function(){
	hideTooltip();
	var $this = $(this), $id = $this.closest(".receipt").attr("data-id"), expense = {_id:getUID(),name:'',sum:0};
	o_receipt.update($id,{field:'expense',value:expense},true);
	var post = {type:'update-receipt',_id:$id,update:{field:'expense',value:expense},userIp:userIp};
	ajaxGlobal('update-receipt',post,'ajax update receipt');
});


// update expense
	// text
$(".expenses-form").on('keyup', ".expense-input-text input",function(e){
	var $this = $(this), $receipt = $this.closest(".receipt"), $expense = $this.closest(".expense")
		, $id = $receipt.attr("data-id"), $expenseid = $expense.attr("data-id"), value = $this.val(); 
	o_receipt.update($id,{id:$expenseid,field:'expense.$',type:'name',value:value},false);
	var post = {type:'update-receipt',_id:$id,expense_id:$expenseid,update:{field:'expense.$',type:'expense.$.name',value:value},userIp:userIp};
	ajaxGlobal('update-receipt',post,'ajax update receipt');
});

	// sum
$(".expenses-form").on('keyup', ".expense-input-sum input",function(e){
	var $this = $(this), $receipt = $this.closest(".receipt"), $expense = $this.closest(".expense"), $id = $receipt.attr("data-id"), $expenseid = $expense.attr("data-id")
		, value = checkNumber($this.val());
	$this.val(value);	
	if (value == ''){ value = 0; };
	o_receipt.update($id,{id:$expenseid,field:'expense.$',type:'sum',value:value},false);
	
	var total_this = o_receipt.total($id,'this'), total = o_receipt.total($id,'all');
	o_receipt.update($id,{field:'total',value:total_this},false);
	o_user.update({field:'total',value:total});
	drawTotal($receipt,total_this,total);
	var post = {type:'update-receipt',_id:$id,expense_id:$expenseid,userId:userId
		,update:{field:'expense.$',type:'expense.$.sum',value:value,total_this:total_this,total:total},userIp:userIp};
	ajaxGlobal('update-receipt',post,'ajax update receipt');
});

//delete
$(".dropdown[data-action='rightclick']").click("[data-id='delete']", function(){
	var $this = $(this), $dropdown = $this.closest(".dropdown"), $id = $dropdown.attr("data-id");	
	closeDropdown();
	o_receipt.delete($id);	
	var total = o_receipt.total('','all');
	o_user.update({field:'total',value:total});
	drawTotal('','',total);
	var post = {type:'delete-receipt',_id:$id,update:{total:total},userId:userId,userIp:userIp}
	ajaxGlobal('delete-receipt',post,'ajax delete receipt');
});


// tooltips
$(".expenses-form").on('mouseover', ".add-receipt", {text:'Add new group',diff_x:-16,diff_y:16,type:'right'}, showTooltip).on('mouseout', ".add-receipt", hideTooltip);

$(".expenses-form").on('mouseover', ".receipt .expense-group", {text:'Change expense group',diff_x:6,diff_y:32,type:'left'}, showTooltip)
	.on('mouseout', ".receipt .expense-group", hideTooltip);

$(".expenses-form").on('mouseover', ".receipt .add-expense", {text:'Add another expense',diff_x:-6,diff_y:24,type:'right'}, showTooltip)
	.on('mouseout', ".receipt .add-expense", hideTooltip);

$(".expenses-form-header [data-action='delete-all']").mouseover({text:'Delete all',diff_x:0,diff_y:32,type:'right'}, showTooltip).mouseout(hideTooltip);

$(".expenses-desc-header .button").mouseover({text:'Show/Hide description',diff_x:0,diff_y:32,type:'left'}, showTooltip).mouseout(hideTooltip);

// delete all
$(".expenses-form-header [data-action='delete-all']").click(function(e){
	hideTooltip();
	o_receipt.delete_all();
	var post = {type:'delete-receipt',_id:'',update:{total:0},userId:userId,userIp:userIp};
	ajaxGlobal('delete-receipt',post,'ajax delete receipt');
});


// choose menu
$(".dropdown[data-action='menu']").on('click', '.dropdown-li', function(){
	var $this = $(this), $type = $this.attr("data-name");
	closeDropdown();
	if ($type == 'List'){
		drawReceipt();
	} else if ($type == 'Chart'){
		let chart = drawChart('pie');
		chart();
	};
});

//choose chart type
$(".dropdown[data-action='chart']").on('click', '.dropdown-li', function(){
	var $this = $(this), $type = $this.attr("data-name");
	closeDropdown();
	if ($type == 'Pie'){
		let chart = drawChart('pie');
		chart();
	} else if ($type == 'Bars'){
		let chart = drawChart('bar');
		chart();
	};
});


// right click menu
$(".main").on('contextmenu', '.receipt', function(e){
	e.preventDefault();
	var receiptId = $(this).attr("data-id"), action = 'rightclick';
	if ($(".dropdown-container[data-action='rightclick']").css('display') == 'none'){
		openDropdown(receiptId,action,{x:e.pageX,y:e.pageY});
	} else {
		closeDropdown();
	};
});


// receipt methods
o_receipt.add = function(obj){
	this.push(obj);
	drawReceipt();
};
o_receipt.update = function(id,field,update){
	var thisobj = this.filter(f => f._id==id)[0];
	if (field.field == 'expense'){
		thisobj[field.field].push(field.value);
	} else if (field.field == 'expense.$'){
		let thisexpense = thisobj.expense;
		thisexpense.filter(f => f._id==field.id)[0][field.type] = field.value;
	} else if (field.field == 'group'){
		thisobj[field.field].name = field.value.name;
		thisobj[field.field].icon = field.value.icon;
	} else {
		thisobj[field.field] = field.value;
	};
	if (update){
		drawReceipt();
	};
};
o_receipt.total = function(id,type){
	var total = 0, initialValue = 0;
	if (type == 'this'){
		var thisobj = this.filter(f => f._id==id)[0];
		total = count_total(thisobj.expense);
	} else if (type == 'all'){
		this.forEach(function(i){
			total += count_total(i.expense);
		});
	};

	function count_total(obj){
		return obj.reduce(function(acc,curr){
			let sum = parseFloat(acc) + parseFloat(curr.sum);
			return sum;
		},initialValue);
	};

	return total;
};
o_receipt.delete = function(id){
	var index = this.findIndex(f => f._id==id);
	this.splice(index,1);
	drawReceipt();
};
o_receipt.delete_all = function(){
	this.length = 0;
	o_user.total = 0;
	drawReceipt();
};

function drawTotal(receipt,total_this,total){
	if (typeof(receipt) === 'object'){
		receipt.find(".receipt-total").html(roundNumber(total_this));
	}; 
	$(".total").find(".total-sum").html(roundNumber(total));
};

function drawReceipt() {
	$(".total .add-receipt").show();
	var html = '';
	o_receipt.forEach(function(i){
		var $template = $("template[data-template='receipt']").prop('content');
		$($template).find(".receipt").attr("data-id",i._id);
		let color = group_list[0].find(f => f.icon==i.group.icon).color;
		$($template).find(".expense-group-icon").html('<i class="material-icons" style="color:'+color+';">'+i.group.icon+'</i>');
		$($template).find(".expense-group-name").html(i.group.name);
		var html_expense = '';
		i.expense.forEach(function(j){
			let $template = $("template[data-template='expense']").prop('content');
			$($template).find(".expense").attr("data-id",j._id);
			$($template).find(".expense-input-text").find("input").attr("value",j.name);
			let sum = j.sum;
			if (sum == 0){ sum = ''; };
			$($template).find(".expense-input-sum").find("input").attr("value",sum);
			html_expense += $("template[data-template='expense']").html();
		});
		$($template).find(".expense-input").html(html_expense);
		$($template).find(".receipt-total").html(roundNumber(i.total));
		html += $("template[data-template='receipt']").html();
	});
	$(".main").html(html);
	$(".total .total-sum").html(roundNumber(o_user.total));
};


// open dropdown
	// menu
$(".expenses-form-header [data-action='menu']").click(function(e){
	var $this = $(this), coords = $this.offset(),  x = Number(coords.left), y = Number(coords.top)+36;
	if ($(".dropdown-container[data-action='menu']").css('display') == 'none'){
		openDropdown('','menu',{x:x,y:y});
	} else {
		closeDropdown();
	};
});
	// chart type
$(".expenses-form").on('click', '.chart-type', function(e){
	var $this = $(this), $dropdown = $(".dropdown-container[data-action='chart']"), coords = $this.offset(),  
		x = Number(coords.left)-$dropdown.width()+30, y = Number(coords.top)+36;
	if ($dropdown.css('display') == 'none'){
		openDropdown('','chart',{x:x,y:y});
	} else {
		closeDropdown();
	};
});

	// group
$(".expenses-form").on('click', '.expense-group', function(e){
	var $this = $(this), receiptId = $this.closest(".receipt").attr("data-id"), action = 'group'
		, x = Number($this.offset().left), y = Number($this.offset().top)+36;
	if ($(".dropdown-container[data-action='group']").css('display') == 'none'){
		openDropdown(receiptId,action,{x:x,y:y});
	} else {
		closeDropdown();
	};
});


$(".expenses-desc-header .button").click(function(){
	hideTooltip();
	var $this = $(this), $text = $this.closest(".expenses-desc-container").find(".expenses-desc");
	$text.toggle();
	$this.find("i").toggleClass("more less");
});
