
function getUID(){
	var dt_h = (+new Date).toString(36);
	var uid = dt_h + Math.random().toString(36).substr(2,10)
	return uid;	
};

function checkNumber(value){
	if(isNaN(value)){
		value = value.replace(/[^0-9\.]/g,'');
		if (value.split('.').length > 2){ 
			value = value.replace(/\.+$/,'');
		};
	};
	return value;
};

function roundNumber(value) {
	return Math.round(value*100)/100;
};


// tooltip
function showTooltip(e){
	var $this = $(this), coords = $this.offset(),  x = Number(coords.left)+e.data.diff_x, y = Number(coords.top)+e.data.diff_y;
	
	$(".tooltip-container .tooltip").html(e.data.text);
	if (e.data.type == 'right'){
		let width = $(".tooltip-container .tooltip").width();
		x = x-width;
	};	
	//setTimeout(function(){
		$(".tooltip-container").css({left:x+'px',top:y+'px',visibility:'visible'});
	//},200);

};
function hideTooltip() {
	$(".tooltip-container").removeAttr('style');
};

// dropdown
function openDropdown(id,action,mouse){
	hideTooltip();
	closeDropdown();
	$(".dropdown[data-action='"+action+"']").attr({'data-id':id});
	$(".dropdown-container[data-action='"+action+"']").css({'left':mouse.x,'top':mouse.y}).slideDown();
};
function closeDropdown(){
	$(".dropdown-container").removeAttr('style');
};

Array.prototype.groupBy = function (property,total){
	var groupby = [];
	this.forEach(function(i,ind){
		var group_ind = groupby.findIndex(f => f[property]==i[property]);
		if (group_ind > -1){
			groupby[group_ind].total += i.total;
			groupby[group_ind].percentage = groupby[group_ind].total/total*100;
		} else {
			groupby.push(i);
		};
	});
	return groupby;
};




