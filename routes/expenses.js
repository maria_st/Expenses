const express = require('express');
const router = express.Router();


// controller modules
const expensesController = require('../controllers/expensesController');


router.get('/', expensesController.expensesG);
router.post('/', expensesController.expensesP);

module.exports = router;