'use strict';
const fs = require('fs');
const logger = fs.createWriteStream('logger.txt', {flags: 'a'});
const ObjectId = require('mongodb').ObjectId;

const group_list = [{id:1,name:'Groceries',icon:'shopping_cart',color:'#ffc107'},{id:2,name:'House',icon:'home',color:'#4caf50'}
	,{id:3,name:'Transportation',icon:'directions_car',color:'#2196f3'},{id:4,name:'Pharmacy',icon:'local_pharmacy',color:'#f44336'}];
const menu = [{name:'List',icon:'format_list_bulleted'}, {name:'Chart',icon:'bubble_chart'}];
const charts = [{name:'Pie',icon:'pie_chart'},{name:'Bars',icon:'bar_charts'}];
const rightclick = [{id:'delete',name:'Delete',icon:'delete'}];


// get
exports.expensesG = function(req, res, next){
	res.render('expenses', {title: 'Expenses',menu:menu,group_list:group_list,charts:charts,rightclick:rightclick});
};

// post
exports.expensesP = function(req, res, next){
	var db = req.app.locals.db, body = req.body, type = body.type;
	if (type == 'onload'){
		onLoad(res,body,db,next);		
	} else if (type == 'add-receipt'){
		addReceipt(res,body,db,next);
	} else if (type == 'update-receipt'){
		updateReceipt(res,body,db);
	} else if (type == 'delete-receipt'){
		deleteReceipt(res,body,db);
	};
};


function onLoad(res,body,db,next){
	logger.write(`\npage load ${new Date(Date.now()).toLocaleString()}, ip = ${body.data.ip}`);
	var users = db.collection('users');
	users.find({ip:body.data.ip}).toArray(function(err, results) {
  		if (results.length == 0){
  			logger.write(`\nnew ip created ${body.data.ip}`);
			users.insert({'ip':body.data.ip,total:0,'city':body.data.city,'country':body.data.country_name,'currency_code':body.data.currency_code},function(err, user){
				if (err) return next(err);
				res.send({user:user.ops,group_list:group_list,receipt:[]});
			});
  		} else {
  			let receipt = db.collection('receipt'), user_id = results[0]._id.toString();
			receipt.find({'user_id':user_id}).toArray(function(err, receipt){
				if (err) return next(err);
				res.send({user:results,group_list:group_list,receipt:receipt});
			});
  		};
	});
};


function addReceipt(res,body,db,next) {
	var receipt = db.collection('receipt');
	receipt.insert(body.receipt,function(err, receipt){
		if (err) return next(err);
		logger.write(`\nadd receipt for user ${body.userIp}`);
		res.send({newreceipt:receipt.ops[0]});
	});	
	
};

function updateReceipt(res,body,db) {
	var receipt = db.collection('receipt'), users = db.collection('users'), query = {_id:body._id}, field = body.update.field;	
	if (field=='expense'){
		receipt.update(query, {$push: {[field]:body.update.value}});
	} else if(field == 'expense.$'){
		let query = {_id:body._id, 'expense._id':body.expense_id};
		receipt.update(query, {$set: {[body.update.type]:body.update.value}});
		if (body.update.type == 'expense.$.sum'){
			receipt.update(query, {$set: {total:body.update.total_this}});
			let users_query = {_id:ObjectId(body.userId)};
			users.update(users_query, {$set: {total:body.update.total}});
		};
	} else if (field=='group'){
		receipt.update(query, {$set: {[field]:{name:body.update.value.name,icon:body.update.value.icon}}});
	} else {
		receipt.update(query, {$set: {[field]:body.update.value}});
	};
	receipt.find(query).toArray(function(err,receiptid){
		if (err) return next(err);
		logger.write(`\nupdate receipt for user ${body.userIp}`);
 		res.send({receiptid: receiptid[0]});
	 });
};

function deleteReceipt(res,body,db){
	var receipt = db.collection('receipt'), users = db.collection('users'), query = {_id:body._id};
	if (body._id == ''){ query = {}; };
	receipt.remove(query);
	let user_query = {_id:ObjectId(body.userId)};
	users.update(user_query, {$set: {total:body.update.total}});
	logger.write(`\ndelete receipt for user ${body.userIp}`);
	res.send({receiptId:body._id});
};




